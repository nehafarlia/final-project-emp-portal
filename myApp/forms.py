from django import forms
from myApp.models import emp_details,leave

class PersonalInfo(forms.ModelForm):
    class Meta():
        model = emp_details
        fields = ('title','date_of_birth','email','a_phone','address','pin_code','bank','bank_account')
        widgets={
            'date_of_birth':forms.DateInput(attrs={'type':'date'})
        }
class HealthInfo(forms.ModelForm):
    class Meta():
        model = emp_details
        fields = ('height','blood_group','weight','disease','left_eye','right_eye')
        widgets={
            'weight':forms.TextInput(attrs={'placeholder':'In Kg'}),
            'height':forms.TextInput(attrs={'placeholder':'In feet'})


        }

class FamilyInfo(forms.ModelForm):
    class Meta():
        model = emp_details
        fields = ('marital_status','spouse_working','children','child_study','father','mother','sibling','income')
        
class Application(forms.ModelForm):
    class Meta():
        model = leave
        fields = ('from_date','to_date','for_days','reason')

class inputsalary(forms.ModelForm):
    class Meta():
        model = emp_details
        fields = ('basic_salary','bonus','total_income','payment_method')

class inputjob(forms.ModelForm):
    class Meta():
        model = emp_details
        fields = ('date_of_probation','position','dept','level','employee_type','holidays','workdays')
        widgets={
            'date_of_probation':forms.DateInput(attrs={'type':'date'})
        }
   